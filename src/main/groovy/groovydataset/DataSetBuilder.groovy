package groovydataset

import org.tools4j.groovytables.GroovyTables
import org.tools4j.groovytables.ConstructionMethodFilter

class DataSetBuilder {
  def tables = []

  def methodMissing(String name, args) {
    def methodMissing = true
    if (args.length == 1) {
      def arg = args[0]
      if (arg instanceof Closure) {
        methodMissing = false
        def table = [:]
        table.name = name
        table.rows = []
        GroovyTables.withTable(arg as Closure).forEachRow {
          def row = [:]
          delegate.metaClass.properties.grep {
            it.name != "class"
          }.each {
            row[it.name] = delegate[it.name]
          }
          table.rows += row
        }
        tables += table
      }
    }
    if (methodMissing) {
      super.methodMissing name, args
    }
  }
}
