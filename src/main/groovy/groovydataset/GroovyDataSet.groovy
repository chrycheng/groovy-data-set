package groovydataset

import org.codehaus.groovy.control.CompilerConfiguration
import org.dbunit.dataset.AbstractDataSet
import org.dbunit.dataset.Column
import org.dbunit.dataset.DefaultTable
import org.dbunit.dataset.DefaultTableIterator
import org.dbunit.dataset.ITable
import org.dbunit.dataset.ITableIterator
import org.dbunit.dataset.datatype.DataType

class GroovyDataSet extends AbstractDataSet {
  private List tables

  GroovyDataSet(source) {
    def config = new CompilerConfiguration()
    config.scriptBaseClass = DataSetScript.name
    def shell = new GroovyShell(this.class.classLoader, config)
    tables = shell.evaluate new InputStreamReader(source)
  }

  protected ITableIterator createIterator(boolean reversed) {
    def tables = []
    this.tables.each { it ->
      def columns = it.rows.first().keySet().collect {
        new Column(it, DataType.UNKNOWN)
      }.toArray new Column[it.rows.first().keySet().size()]
      def table = new DefaultTable(it.name, columns)
      it.rows.each { row ->
        def values = []
        columns.each { column ->
          values += row[column.columnName]
        }
        table.addRow values.toArray(new Object[values.size()])
      }
      tables += table
    }
    return new DefaultTableIterator(tables.toArray(new ITable[tables.size()]), reversed)
  }
}
