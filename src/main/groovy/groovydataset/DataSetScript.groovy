package groovydataset

abstract class DataSetScript extends Script {
  def dataSet(@DelegatesTo(strategy = Closure.DELEGATE_ONLY , value = DataSetBuilder) closure) {
    def dataSetBuilder = new DataSetBuilder()
    def code = closure.rehydrate dataSetBuilder, this, this
    code.resolveStrategy = Closure.DELEGATE_ONLY
    code()
    return dataSetBuilder.tables
  }
}
