package groovydataset

import spock.lang.Specification

class GroovyDataSetSpec extends Specification {
  def "should provide iterator over data set"() {
    given:
    def source = openTestResource "/simple.groovy"
    def groovyDataSet = new GroovyDataSet(source)
    def tableIterator = groovyDataSet.createIterator false
    tableIterator.next()

    expect:
    tableIterator.table.getValue(row, "column1") == column1
    tableIterator.table.getValue(row, "column2") == column2
    tableIterator.table.getValue(row, "column3") == column3

    where:
    row | column1          | column2          | column3
      0 | "column 1 row 1" | "column 2 row 1" | "column 3 row 1"
      1 | "column 1 row 2" | "column 2 row 2" | "column 3 row 2"
      2 | "column 1 row 3" | "column 2 row 3" | "column 3 row 3"
  }

  def testResources = []

  def openTestResource(name) {
    def testResource = this.class.getResourceAsStream name
    testResources += testResource
    return testResource
  }

  def cleanup() {
    testResources.each { it.close() }
  }
}
