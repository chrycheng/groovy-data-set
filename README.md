# Groovy Data Set

A [DbUnit](http://dbunit.sourceforge.net/) plug-in to allow the use of
[Groovy](http://groovy-lang.org/) to define a data set.

## Usage

1.  Add `groovy-data-set` as an internal test compilation dependency.
    E.g., in Gradle:

    ```groovy
    dependencies {
      testImplementation "com.gitlab.chrycheng:groovy-data-set:1.0.0"
    }
    ```

2.  Create your data set file:

    ```groovy
    dataSet {
      users {
        id | name         | email
         0 | "John Smith" | "johnsmith@fake.com"
         1 | "Jane Smith" | "janesmith@bogus.com"
      }
      posts {
        id | title             | body                                                                   | author
         0 | "On things"       | "Lorem ipsum dolor sit amet, consectetur adipiscing elit."             |      0
         1 | "About the thing" | "Integer sodales tellus ac lacus euismod, quis suscipit elit egestas." |      1
         2 | "How to do stuff" | "Praesent a tortor ac ligula eleifend molestie efficitur nec ex."      |      1
      }
    }
    ```

3.  Obtain a `java.io.InputStream` of it:

    ```java
    try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream("/data-sets/posts.groovy")) {
      // ...
    }
    ```

4.  Instantiate a `groovydataset.GroovyDataSet` with it:

    ```java
    IDataSet dataSet = new GroovyDataSet(inputStream);
    ```

## Acknowledgement

Practically just a wrapper around the excellent [Groovy Tables](https://github.com/tools4j/groovy-tables)
library.
